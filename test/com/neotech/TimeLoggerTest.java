package com.neotech;

import org.junit.Assert;
import org.junit.Test;

import java.sql.Time;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

public class TimeLoggerTest {



    @Test
    public void makeList() {
        TimeLogger timeLogger = new TimeLogger();
        int before = timeLogger.timestamps.size()+1;
        timeLogger.makeList();
        int after = timeLogger.timestamps.size();
        Assert.assertEquals(before, after);
    }

    @Test
    public void writeList(Resource resource) {
        resource = mock(Resource.class);
        TimeLogger timeLogger = new TimeLogger();
        timeLogger.timestamps.add("one");
        timeLogger.timestamps.add("two");
        doNothing().when(resource).insertLineToDB(any());
        writeList(resource);

        Assert.assertEquals(timeLogger.timestamps.size(), 0);

    }
}