package com.neotech;

import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ViewTest {

    @Test
    public void viewTableContent() {
        PrintStream out = mock(PrintStream.class);
        System.setOut(out);
        List<String> preparedList = new ArrayList<String>(Arrays.asList("one", "two", "three"));
        View view = new View();
        view.viewTableContent(preparedList);

        verify(out).println("Table content: \none\ntwo\nthree");
    }
}