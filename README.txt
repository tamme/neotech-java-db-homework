Write a console java application with the following functions:
1. when running without parameters, the application starts writing the current time every second
(timestamp) to the database.
2. When running with the -p option, the application displays the contents of the database on the screen and terminates
the work


It is required to ensure that the application works correctly when
temporary unavailability of the database server - the application must
To display in a broad gull or on the screen the information on that connection is not present, and
attempt to reconnect, and then
restore the connection to write down all the data that is not in the
DB in the absence of communication.

Connection recovery interval is 5 seconds.
All data must be recorded in the DB strictly in chronological order,
those. When displaying data on the screen, the timestamp must be in the Ascending Order without applying
sorting.

It is also necessary to consider and, accordingly, "answer" the question (via code):
what will happen if the connection to the database is slow or the database is slow
so loaded that the record in it will take a lot of time.


Database to choose from: MySQL, MongoDB.
Method of data storage in the database: at the discretion of the author.