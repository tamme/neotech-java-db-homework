package com.neotech;

public class Main {

    public static void main(String[] args) {
        String option = "";

        try {
            option = args[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            option = "";
        } finally {

            if ("-p".equals(option)) {
                View view = new View();
                view.viewTableContent(view.tableContent);
            } else if ("".equals(option)) {
                TimeLogger timelogger = new TimeLogger();
                timelogger.multiThread();

            }
        }
    }
}
