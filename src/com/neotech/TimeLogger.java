package com.neotech;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TimeLogger {
    List<String> timestamps = new ArrayList<>();

    void multiThread() {
        Thread thread1 = new Thread() {
            public void run() {
                while (true) {
                    makeList();
                }
            }
        };

        Thread thread2 = new Thread() {
            public void run() {
                Resource resource = new Resource();
                while (true) {
                    writeList(resource);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }
                }
            }
        };
        thread1.start();
        thread2.start();
    }


    void makeList() {
        this.timestamps.add(String.valueOf(LocalDateTime.now()));
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    void writeList(Resource resource) {
            for (int i = 0; i < timestamps.size(); i++) {
                resource.insertLineToDB(timestamps.get(0));
                timestamps.remove(0);
            }
    }
}


