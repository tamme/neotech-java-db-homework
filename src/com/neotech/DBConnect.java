package com.neotech;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class DBConnect {
    public Connection connection;
    private String user;
    private String password;
    private String url;
    private String driver;

    public DBConnect() throws IOException, InterruptedException {
        getCredientals();
    }

    private void getCredientals() throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(new File("src/../credentials.properties")));
        this.user = properties.getProperty("username");
        this.password = properties.getProperty("password");
        this.url = properties.getProperty("url");
        this.driver = properties.getProperty("driver");
    }

    public Connection createConnection() throws InterruptedException {
        boolean disconnected = true;
        while (disconnected) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection(this.url, this.user, this.password);
                disconnected = false;
            } catch (Exception e) {
                System.out.println(e + "Database is not accessible, trying again in 5 sec.");
                TimeUnit.SECONDS.sleep(5);
            }
        }
        return connection;
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            System.out.println(e + " Error closing database connection!");
        }
    }
}
