package com.neotech;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Resource {
    DBConnect databaseConnection;

    public Resource() {
        try {
            this.databaseConnection = new DBConnect();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<String> getData() {
        String query = "SELECT * FROM time";
        List<String> tableContent = new ArrayList<>();
        try {

            ResultSet result = databaseConnection.createConnection().createStatement().executeQuery(query);
            while (result.next()) {
                String timestamp = result.getString("timestamp");
                tableContent.add(timestamp);
            }
            databaseConnection.closeConnection();
            return tableContent;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return tableContent;
    }

    public void insertLineToDB(String timestamp) {

        String query = "INSERT INTO time (timestamp) VALUES (?)";

        try (PreparedStatement preparedStatement = databaseConnection.createConnection().prepareStatement(query)) {

            preparedStatement.setString(1, timestamp);
            preparedStatement.execute();
            databaseConnection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
