package com.neotech;

import java.util.List;

public class View {
    Resource resource;
    List<String> tableContent;

    public View() {
        this.resource = new Resource();
        this.tableContent = this.resource.getData();
    }

    public void viewTableContent(List<String> tableContent) {

        String tableContentAsString = new String("Table content: ");
        for (String row : tableContent) {
            tableContentAsString = tableContentAsString + "\n" + row;

        }
        System.out.println(tableContentAsString);
    }
}
